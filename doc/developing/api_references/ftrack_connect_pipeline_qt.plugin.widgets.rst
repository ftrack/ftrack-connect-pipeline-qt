ftrack\_connect\_pipeline\_qt.plugin.widgets package
====================================================

.. automodule:: ftrack_connect_pipeline_qt.plugin.widgets
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

ftrack\_connect\_pipeline\_qt.plugin.widgets.base_collector_widget module
-------------------------------------------------------------------------

.. automodule:: ftrack_connect_pipeline_qt.plugin.widgets.base_collector_widget
   :members:
   :undoc-members:
   :show-inheritance:

ftrack\_connect\_pipeline\_qt.plugin.widgets.context module
-----------------------------------------------------------

.. automodule:: ftrack_connect_pipeline_qt.plugin.widgets.context
   :members:
   :undoc-members:
   :show-inheritance:

ftrack\_connect\_pipeline\_qt.plugin.widgets.dynamic module
-----------------------------------------------------------

.. automodule:: ftrack_connect_pipeline_qt.plugin.widgets.dynamic
   :members:
   :undoc-members:
   :show-inheritance:

ftrack\_connect\_pipeline\_qt.plugin.widgets.load_widget module
---------------------------------------------------------------

.. automodule:: ftrack_connect_pipeline_qt.plugin.widgets.load_widget
   :members:
   :undoc-members:
   :show-inheritance:

ftrack\_connect\_pipeline\_qt.plugin.widgets.open_widget module
---------------------------------------------------------------

.. automodule:: ftrack_connect_pipeline_qt.plugin.widgets.open_widget
   :members:
   :undoc-members:
   :show-inheritance:

