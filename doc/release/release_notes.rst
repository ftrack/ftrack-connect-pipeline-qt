..
    :copyright: Copyright (c) 2022 ftrack

.. _release/release_notes:

*************
Release Notes
*************

.. release:: upcoming

    .. change:: change
        :tags: dynamicwidget

        Finalised Dynamic widget . list / combobox handling.


    .. change:: change
        :tags: dynamicwidget

        Dynamic widget renders widgets within a group box instead of using the default redundant plugin widget label.


    .. change:: change
        :tags: overlay

        Updated the visual appearance of options overlay, removed accordion use.


    .. change:: fixed
        :tags: overlay

        Fixed further overlay event filter warnings


    .. change:: fixed
        :tags: context

        Align with changes in pipeline context workflow


    .. change:: fixed

        Event filter warnings in Nuke and Maya


    .. change:: fixed

        Fixed assembler version selector bug caused by previous opener changes


    .. change:: fixed
        :tags: doc

        Fixed bug where opener definition selector could not spot an openable version


    .. change:: change

         Removed version id from asset list event.


    .. change:: change

        Passing version ID from version selection instead of Version API object


    .. change:: change

        Prevent opener from listing and opening incompatible snapshots


.. release:: 1.0.1
    :date: 2022-08-01

    .. change:: new

        Initial release

